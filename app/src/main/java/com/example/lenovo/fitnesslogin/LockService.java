package com.example.lenovo.fitnesslogin;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by LENOVO on 07-12-2017.
 */
public class LockService extends Service{

    DatabaseReference db,dbchild;
    String EndTime;
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    BroadcastReceiver mReceiver;

    @Override
    public void onCreate() {

        super.onCreate();
        // register receiver that handles screen on and screen off logic
        IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        mReceiver = new ScreenReceiver();
        registerReceiver(mReceiver, filter);
       EndTime = new SimpleDateFormat("h:mm a").format(new Date());

        String date = new SimpleDateFormat("d-MM-yyyy").format(new Date());
        String id = FirebaseAuth.getInstance().getCurrentUser().getUid();
        db  = FirebaseDatabase.getInstance().getReference("Progress Report").child(id);
        dbchild = db.child(date).child("Sleep Tracker");

    }

    @Override
    public void onDestroy() {

        unregisterReceiver(mReceiver);
        //Log.i("onDestroy Reciever", "Called");

        super.onDestroy();
    }

    @Override
    public void onStart(Intent intent, int startId) {
        boolean screenOn = intent.getBooleanExtra("screen_state", false);
        if (!screenOn) {
            // Log.i("screenON", "Called");


            EndTime = new SimpleDateFormat("h:mm a").format(new Date());
            Toast.makeText(getApplicationContext(), "Your Sleep Tracker is Running", Toast.LENGTH_LONG)
                              .show();

            dbchild.child("End Time").setValue(EndTime);


        } else {
            // Log.i("screenOFF", "Called");

        }
    }

}
