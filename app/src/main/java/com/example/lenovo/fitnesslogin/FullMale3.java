package com.example.lenovo.fitnesslogin;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

public class FullMale3 extends AppCompatActivity {

    ImageView img;
    FloatingActionButton floatingActionButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_male3);

        floatingActionButton = (FloatingActionButton) findViewById(R.id.fb_trishape3);
        img = (ImageView) findViewById(R.id.trishape3);
        String url = "https://firebasestorage.googleapis.com/v0/b/project--5985293472804418431.appspot.com/o/image%2FMaleWorkout%2FMaleFullBody%2FTriShapes.gif?alt=media&token=b3755edf-a7ef-41a8-b921-ca55fece3029";
        Glide.with(getApplicationContext()).load(url).into(img);

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(getApplicationContext(),"Floating Button",Toast.LENGTH_LONG).show();
                startActivity(new Intent(getApplicationContext(),FullMale4.class));
            }
        });
    }
}
