package com.example.lenovo.fitnesslogin;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

public class FullMale1 extends AppCompatActivity {

    ImageView img;
    FloatingActionButton fab;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_male1);

        fab = (FloatingActionButton) findViewById(R.id.fb_chest1);
        img = (ImageView) findViewById(R.id.chest1);
        String url = "https://firebasestorage.googleapis.com/v0/b/project--5985293472804418431.appspot.com/o/image%2FMaleWorkout%2FMaleFullBody%2FChestPressPulse.gif?alt=media&token=e8914b3e-cb73-4ccc-8a0f-b45076e22938";
        Glide.with(getApplicationContext()).load(url).into(img);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),FullMale2.class));
                //Toast.makeText(getApplicationContext(),"Floating Button",Toast.LENGTH_LONG).show();
            }
        });
    }
}
