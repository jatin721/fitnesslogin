package com.example.lenovo.fitnesslogin;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

public class JnanaMainAct extends AppCompatActivity {

    FloatingActionButton fab;
    ImageView img;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jnana_main);


        img = (ImageView) findViewById(R.id.dhanurasana);
        fab = (FloatingActionButton) findViewById(R.id.fb_dhanu);
        String url = "https://firebasestorage.googleapis.com/v0/b/project--5985293472804418431.appspot.com/o/image%2FYogaAct%2FJnanaYoga%2Fdhanurasana.png?alt=media&token=67e2cb33-da13-4fdf-9442-c82a9b18a212";
        Glide.with(getApplicationContext()).load(url).into(img);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),JnanaAct1.class));
            }
        });
    }
}
