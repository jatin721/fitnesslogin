package com.example.lenovo.fitnesslogin;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ActivityArm2 extends AppCompatActivity {

    ImageView img;
    FloatingActionButton floatingActionButton;
    DatabaseReference databaseReference,dbchild;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_arm2);


        floatingActionButton = (FloatingActionButton) findViewById(R.id.fb_trishape);
        img = (ImageView) findViewById(R.id.trishape);
        String url = "https://firebasestorage.googleapis.com/v0/b/project--5985293472804418431.appspot.com/o/image%2FMaleWorkout%2FMaleArm%2FTriShapes.gif?alt=media&token=a02ab70b-907a-4111-b00a-ecf0465582a7";
        Glide.with(getApplicationContext()).load(url).into(img);

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(getApplicationContext(),"Floating Button",Toast.LENGTH_LONG).show();
                startActivity(new Intent(getApplicationContext(),ActivityArm3.class));
            }
        });
    }
}
