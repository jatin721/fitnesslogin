package com.example.lenovo.fitnesslogin;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

public class TanthaAct3 extends AppCompatActivity {

    FloatingActionButton fab;
    ImageView img;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tantha_act3);

        img = (ImageView) findViewById(R.id.pavanmukthasana);
        fab = (FloatingActionButton) findViewById(R.id.fb_pavan);
        String url = "https://firebasestorage.googleapis.com/v0/b/project--5985293472804418431.appspot.com/o/image%2FYogaAct%2FTanthaYoga%2Fpavanmukhtasana.png?alt=media&token=84dcf2bd-bbd3-4693-b670-f5561d4a931e";
        Glide.with(getApplicationContext()).load(url).into(img);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),TanthaAct4.class));
            }
        });
    }
}
