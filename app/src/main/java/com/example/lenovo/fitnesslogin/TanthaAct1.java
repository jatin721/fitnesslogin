package com.example.lenovo.fitnesslogin;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

public class TanthaAct1 extends AppCompatActivity {


    FloatingActionButton fab;
    ImageView img;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tantha_act1);

        img = (ImageView) findViewById(R.id.janusirasanaright);
        fab = (FloatingActionButton) findViewById(R.id.fb_januright);
        String url = "https://firebasestorage.googleapis.com/v0/b/project--5985293472804418431.appspot.com/o/image%2FYogaAct%2FTanthaYoga%2FjanusirasanaRight.png?alt=media&token=b78c8d95-7b2d-4b52-98ec-a4bb39e629e0";
        Glide.with(getApplicationContext()).load(url).into(img);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),TanthaAct2.class));
            }
        });
    }
}
