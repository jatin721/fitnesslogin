package com.example.lenovo.fitnesslogin;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class FemaleMainAct extends AppCompatActivity {

    ListView listView1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_female_main);

        listView1 = (ListView) findViewById(R.id.ladylist);
        String[] data = {"Arm Workout", "Abs Workout","Leg Workout","Full body Workout"};

        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_list_item_1,data){
            @NonNull
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);

                // Initialize a TextView for ListView each Item
                TextView tv = (TextView) view.findViewById(android.R.id.text1);

                // Set the text color of TextView (ListView Item)
                tv.setTextColor(Color.WHITE);

                // Generate ListView Item using TextView
                return view;
            }
        };
        listView1.setAdapter(adapter1);


        listView1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if(i == 0)
                {
                    startActivity(new Intent(getApplicationContext(),FemaleArmAct1.class));


                }else if(i == 1)
                {

                    startActivity(new Intent(getApplicationContext(),FemaleAbsAct1.class));

                }else if(i == 2)
                {
                    startActivity(new Intent(getApplicationContext(),FemaleLegAct1.class));

                }
                else if(i == 3)
                {
                    startActivity(new Intent(getApplicationContext(),FullFemale1.class));
                }

            }
        });



    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        startActivity(new Intent(getApplicationContext(),MyActivity.class));
    }
}
