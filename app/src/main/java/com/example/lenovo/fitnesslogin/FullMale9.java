package com.example.lenovo.fitnesslogin;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DatabaseReference;

public class FullMale9 extends AppCompatActivity {

    ImageView img;
    FloatingActionButton fab;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_male9);

        fab = (FloatingActionButton) findViewById(R.id.fb_revcrunch9);
        img = (ImageView) findViewById(R.id.revcrunch9);
        String url = "https://firebasestorage.googleapis.com/v0/b/project--5985293472804418431.appspot.com/o/image%2FMaleWorkout%2FMaleFullBody%2Frev_crunches.gif?alt=media&token=d2405a4b-460c-4102-ac24-39c757f8457e";
        Glide.with(getApplicationContext()).load(url).into(img);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),FullMale10.class));
            }
        });
    }
}
