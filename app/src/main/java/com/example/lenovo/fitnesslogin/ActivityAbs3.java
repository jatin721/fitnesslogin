package com.example.lenovo.fitnesslogin;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

public class ActivityAbs3 extends AppCompatActivity {


    ImageView img;
    FloatingActionButton fab;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_abs3);


        fab = (FloatingActionButton) findViewById(R.id.fb_longcrunch);
        img = (ImageView) findViewById(R.id.longcrunch);
        String url = "https://firebasestorage.googleapis.com/v0/b/project--5985293472804418431.appspot.com/o/image%2FMaleWorkout%2FMaleAbs%2FLongArmCrunch.gif?alt=media&token=00ddf9bf-c0b1-434e-bfe2-9f0604ac1ef3";
        Glide.with(getApplicationContext()).load(url).into(img);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(getApplicationContext(),ActivityAbs4.class));
            }
        });
    }
}
