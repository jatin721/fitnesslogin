package com.example.lenovo.fitnesslogin;

/**
 * Created by jatin on 05-11-2017.
 */

public class ImageUpload {

    public String name;
    public String url;

    public String getUrl() {
        return url;
    }

    public String getName() {
        return name;
    }

    public ImageUpload(String name, String url) {
        this.name = name;
        this.url = url;
    }
}
