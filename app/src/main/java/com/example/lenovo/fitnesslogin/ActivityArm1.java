package com.example.lenovo.fitnesslogin;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

public class ActivityArm1 extends AppCompatActivity {

    ImageView img;
    FloatingActionButton floatingActionButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_arm1);
        floatingActionButton = (FloatingActionButton) findViewById(R.id.fb_chest);
        img = (ImageView) findViewById(R.id.chest);
        String url = "https://firebasestorage.googleapis.com/v0/b/project--5985293472804418431.appspot.com/o/image%2FMaleWorkout%2FMaleArm%2FChestPressPulse.gif?alt=media&token=edeaebac-66aa-4478-bdac-cec983510833";
        Glide.with(getApplicationContext()).load(url).into(img);

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),ActivityArm2.class));
                //Toast.makeText(getApplicationContext(),"Floating Button",Toast.LENGTH_LONG).show();
            }
        });
    }




}


