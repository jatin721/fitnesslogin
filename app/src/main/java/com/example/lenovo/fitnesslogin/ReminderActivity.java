package com.example.lenovo.fitnesslogin;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.icu.util.Calendar;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TimePicker;
import android.widget.Toast;

public class ReminderActivity extends AppCompatActivity {

    TimePicker timePicker;

    PendingIntent pendingIntent;
    AlarmManager manager;
    Calendar calendar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminder);

        timePicker = (TimePicker) findViewById(R.id.timePicker);



    }


    public void setReminder(View view)
    {
        int hour = timePicker.getCurrentHour();
       int min =  timePicker.getCurrentMinute();

        calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, min);
        calendar.set(Calendar.SECOND, 0);
        Intent intent1 = new Intent(ReminderActivity.this, AlarmReceiver.class);

        pendingIntent = PendingIntent.getBroadcast(ReminderActivity.this, 0,intent1, PendingIntent.FLAG_UPDATE_CURRENT);
        manager = (AlarmManager) ReminderActivity.this.getSystemService(ReminderActivity.this.ALARM_SERVICE);
        manager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);

        Toast.makeText(this, "Reminder Set", Toast.LENGTH_SHORT).show();


        }


    }

