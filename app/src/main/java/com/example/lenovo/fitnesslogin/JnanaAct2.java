package com.example.lenovo.fitnesslogin;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

public class JnanaAct2 extends AppCompatActivity {


    FloatingActionButton fab;
    ImageView img;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jnana_act2);

        img = (ImageView) findViewById(R.id.ardhabhujangasana);
        fab = (FloatingActionButton) findViewById(R.id.fb_ardhdhanubhujang);
        String url = "https://firebasestorage.googleapis.com/v0/b/project--5985293472804418431.appspot.com/o/image%2FYogaAct%2FJnanaYoga%2FardhaBhujangasana.png?alt=media&token=b3de01b7-0afd-4416-9e94-41aa9eec33d0";
        Glide.with(getApplicationContext()).load(url).into(img);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),JnanaAct3.class));
            }
        });
    }
}
