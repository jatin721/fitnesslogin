package com.example.lenovo.fitnesslogin;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

public class HathaAct1 extends AppCompatActivity {

    FloatingActionButton fab;
    ImageView img;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hatha_act1);

        img = (ImageView) findViewById(R.id.ardhaChatrunga);
        fab = (FloatingActionButton) findViewById(R.id.fb_ardhach);
        String url = "https://firebasestorage.googleapis.com/v0/b/project--5985293472804418431.appspot.com/o/image%2FYogaAct%2FHathaYoga%2FardhaChatrunga.png?alt=media&token=1db35a4b-5330-4e7d-a5d5-c2cd56e31d95";
        Glide.with(getApplicationContext()).load(url).into(img);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),HathaAct2.class));
            }
        });
    }
}
