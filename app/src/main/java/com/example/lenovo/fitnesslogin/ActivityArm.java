package com.example.lenovo.fitnesslogin;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

public class ActivityArm extends AppCompatActivity {

    FloatingActionButton fab;
    ImageView img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_arm);

        img = (ImageView) findViewById(R.id.pushup);
        fab = (FloatingActionButton) findViewById(R.id.fb_pushup);
        String url = "https://firebasestorage.googleapis.com/v0/b/project--5985293472804418431.appspot.com/o/image%2FMaleWorkout%2FMaleArm%2Fpushup.gif?alt=media&token=7bfe8ba2-dcd6-4ad8-88ad-11a880d20fcb";
        Glide.with(getApplicationContext()).load(url).into(img);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), ActivityArm1.class));
            }
        });
    }


   }

