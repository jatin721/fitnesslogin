package com.example.lenovo.fitnesslogin;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DatabaseReference;

public class FullFemale5 extends AppCompatActivity {

    ImageView img;
    FloatingActionButton fab;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_female5);

        fab = (FloatingActionButton) findViewById(R.id.fb_fplankright5);
        img = (ImageView) findViewById(R.id.femaleplankright5);
        String url = "https://firebasestorage.googleapis.com/v0/b/project--5985293472804418431.appspot.com/o/image%2FFemaleWorkout%2FFullbody%2FSide-plank(Right).gif?alt=media&token=ec4eb216-852b-4b19-b2d8-104c311aa0eb";
        Glide.with(getApplicationContext()).load(url).into(img);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),FullFemale6.class));
            }
        });
    }
}
