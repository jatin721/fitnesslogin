package com.example.lenovo.fitnesslogin;

import android.app.KeyguardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.Date;

public class SleepActivity extends AppCompatActivity {


    TextView textView;
    ToggleButton btnToggleLock;
    Toast toast;
    int Starthour,Startmin,Stophour,hours;
    String StartTime;


    DatabaseReference db,dbchild;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sleep);


        String date = new SimpleDateFormat("d-MM-yyyy").format(new Date());
        String id = FirebaseAuth.getInstance().getCurrentUser().getUid();
        db  = FirebaseDatabase.getInstance().getReference("Progress Report").child(id);
        dbchild = db.child(date).child("Sleep Tracker");


        textView = (TextView) findViewById(R.id.textView2);
        btnToggleLock = (ToggleButton) findViewById(R.id.toggleButton1);

        toast = Toast.makeText(getApplicationContext(), "", Toast.LENGTH_SHORT);

        btnToggleLock.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (btnToggleLock.isChecked()) {


                    toast.cancel();
                    toast.setText("Unlocked");
                    toast.show();
                    textView.setText("Click this button to stop recording");
                    Starthour= Integer.parseInt(new SimpleDateFormat("HH").format(new Date()));
                    //Startmin = Integer.parseInt(new SimpleDateFormat("mm").format(new Date()));
                    StartTime = new SimpleDateFormat("h:mm a").format(new Date());



                    dbchild.child("Start Time").setValue(StartTime);
                    // Log.i("Unlocked", "If");

                    Context context = getApplicationContext();
                    KeyguardManager _guard = (KeyguardManager) context
                            .getSystemService(Context.KEYGUARD_SERVICE);
                    KeyguardManager.KeyguardLock _keyguardLock = _guard
                            .newKeyguardLock("KeyguardLockWrapper");
                    _keyguardLock.disableKeyguard();

                    SleepActivity.this.startService(new Intent(
                            SleepActivity.this, LockService.class));

                } else {

                    toast.cancel();
                    toast.setText("Locked");
                    toast.show();
                    textView.setText("Click this button to start recording your sleep");

                        Stophour =Integer.parseInt(new SimpleDateFormat("HH").format(new Date()));;
                   if(Starthour>12&&Stophour<12) {
                       hours = (24 - Starthour) + (Stophour - 00);


                       dbchild.child("Sleep Hours").setValue(hours);

                   }
                    else
                   {
                       hours = (Stophour - Starthour);

                       dbchild.child("Sleep Hours").setValue(hours);

                   }

                    Toast.makeText(getApplicationContext(),"Your Data Saved",Toast.LENGTH_LONG).show();
                    Context context = getApplicationContext();
                    KeyguardManager _guard = (KeyguardManager) context
                            .getSystemService(Context.KEYGUARD_SERVICE);
                    KeyguardManager.KeyguardLock _keyguardLock = _guard
                            .newKeyguardLock("KeyguardLockWrapper");
                    _keyguardLock.reenableKeyguard();

                    // Log.i("Locked", "else");

                    SleepActivity.this.stopService(new Intent(SleepActivity.this,
                            LockService.class));

                }

            }
        });

    }

}

