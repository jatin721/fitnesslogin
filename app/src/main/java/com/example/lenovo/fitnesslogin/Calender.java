package com.example.lenovo.fitnesslogin;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.CalendarView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


public class Calender extends AppCompatActivity {

    //ListView listView;
    TextView textView;
    DatabaseReference dbref,dbchild;
    String date;
    CalendarView calendarView;
    String dat;
    //ArrayList<Integer> arrayList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calender);
        //listView = (ListView) findViewById(R.id.mylist);
        textView = (TextView) findViewById(R.id.myText);
        calendarView = (CalendarView) findViewById(R.id.calendarView);
        String email = FirebaseAuth.getInstance().getCurrentUser().getUid();
        dbref = FirebaseDatabase.getInstance().getReference("Progress Report").child(email);


        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView calendarView, int i, int i1, int i2) {

                dat = i2+"-"+(i1+1)+"-"+i;
                dbchild=dbref.child(dat);

                dbchild.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        StringBuffer stringBuffer = new StringBuffer();

                        if(dataSnapshot.exists())
                        {
                           if(dataSnapshot.child("Step Tracker").exists()) {

                                int value = dataSnapshot.child("Step Tracker").child("Steps").getValue(int.class);
                                int value2 = dataSnapshot.child("Step Tracker").child("Calories").getValue(int.class);
                                stringBuffer.append("Step Tracker\nSteps :" + value + "\nCalories :" + value2);

                            }

                            if(dataSnapshot.child("Sleep Tracker").child("Sleep Hours").exists())
                            {
                                String startTime = dataSnapshot.child("Sleep Tracker").child("Start Time").getValue(String.class);
                                String endTime = dataSnapshot.child("Sleep Tracker").child("End Time").getValue(String.class);
                                int sleepHours = dataSnapshot.child("Sleep Tracker").child("Sleep Hours").getValue(int.class);
                                stringBuffer.append("\n\nSleep Tacker\nStart Time :"+startTime+"\nEnd Time :"+endTime+"\nSleep Hours :"+sleepHours);

                            }

                            if(dataSnapshot.child("Activity").exists())
                            {
                                StringBuffer activityStatus = new StringBuffer();
                               for(DataSnapshot data :dataSnapshot.child("Activity").getChildren())
                               {

                                   activityStatus.append(data.getKey()+"\n");
                               }

                                stringBuffer.append("\n\nActivity Completed\n"+activityStatus);


                            }

                            textView.setText(stringBuffer.toString());
                        }
                        else {
                            textView.setText("No Data");
                        }

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });


            }
        });


    }
}
