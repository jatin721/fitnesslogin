package com.example.lenovo.fitnesslogin;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

public class HathaAct3 extends AppCompatActivity {

    FloatingActionButton fab;
    ImageView img;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hatha_act3);



        img = (ImageView) findViewById(R.id.dandasana);
        fab = (FloatingActionButton) findViewById(R.id.fb_dandasana);
        String url = "https://firebasestorage.googleapis.com/v0/b/project--5985293472804418431.appspot.com/o/image%2FYogaAct%2FHathaYoga%2Fdandasana.png?alt=media&token=cce2ef05-f645-471e-839f-2005ded390f5";
        Glide.with(getApplicationContext()).load(url).into(img);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),HathaAct4.class));
            }
        });
    }
}
