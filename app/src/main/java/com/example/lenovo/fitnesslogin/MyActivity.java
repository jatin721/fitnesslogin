package com.example.lenovo.fitnesslogin;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import de.hdodenhof.circleimageview.CircleImageView;

public class MyActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    TabLayout myTab;
    ViewPager myPager;
    //rivate FirebaseAuth firebaseAuth;
    TextView textView,textView2;
    Toolbar toolbar;
    DatabaseReference db;
    View header;
    NavigationView navigationView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View _mw = getLayoutInflater().inflate(R.layout.activity_my2, null);
        setContentView(_mw);



        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
       header=navigationView.getHeaderView(0);


        TextView email = (TextView) header.findViewById(R.id.txt_header);
        email.setText(""+FirebaseAuth.getInstance().getCurrentUser().getEmail());


        db = FirebaseDatabase.getInstance().getReference("Profile pictures").child(FirebaseAuth.getInstance().getCurrentUser().getUid());
        db.addValueEventListener(new ValueEventListener() {
            CircleImageView imageView = (CircleImageView) header.findViewById(R.id.propic);
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.child("url").exists()) {

                    String url = dataSnapshot.child("url").getValue(String.class);
                    Glide.with(getApplicationContext()).load(url).into(imageView);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });




        myTab = (TabLayout) findViewById(R.id.myTab);
        myPager = (ViewPager) findViewById(R.id.myPager);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        DrawerLayout drawerLayout = (DrawerLayout) findViewById(R.id.drawerlayout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.open, R.string.close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();


        myPager.setAdapter(new MyOwnPagerFragmentAdapter(getSupportFragmentManager()));
        myTab.setupWithViewPager(myPager);


        myTab.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                myPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.profile:
               // Toast.makeText(this, "Profile", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(),UserProfile.class));

                break;

            case R.id.step_tracker:
                //Toast.makeText(this, "Step Tracker", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(),StepActivity.class));

                break;
            case R.id.calenderdata:
                //Toast.makeText(this, "Calender", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(),Calender.class));

                break;

            case R.id.sleep_tracker:
                //Toast.makeText(this, "Sleep Tracker", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(),SleepActivity.class));
                break;
            case R.id.female:
                //Toast.makeText(this, "Sleep Tracker", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(),FemaleMainAct.class));
                break;
            case R.id.yoga:
               // Toast.makeText(this, "Sleep Tracker", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(),YogaMainAct.class));
                break;
            case R.id.feedback:
               // Toast.makeText(this, "Feedback", Toast.LENGTH_SHORT).show();



                Intent choose;
                Intent   intent = new Intent(Intent.ACTION_SEND);

                intent.setData(Uri.parse("mailto:"));
                String[] to = {"jatinparmar721@gmail.com"};

                intent.putExtra(intent.EXTRA_EMAIL, to);


                intent.setType("message/rfc822");

                choose = intent.createChooser(intent, "Send Feedback");
                startActivity(choose) ;



                break;

            case R.id.reminder:
                // Toast.makeText(this, "Sleep Tracker", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(),ReminderActivity.class));
                break;


        }
        DrawerLayout drawerLayout = (DrawerLayout) findViewById(R.id.drawerlayout);
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;

    }

    class MyOwnPagerFragmentAdapter extends FragmentPagerAdapter {

        String data[] = {"Workout", "Instruction"};

        public MyOwnPagerFragmentAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                return new Workout();
            }
            if (position == 1) {
                return new Instruction();

            }

            return null;
        }


        @Override
        public int getCount() {
            return data.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return data[position];
        }

    }

    @Override
    public void onBackPressed() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(MyActivity.this);
        builder.setMessage("Do you want to Exit ?");
        builder.setCancelable(false);

        builder.setNegativeButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();

            }
        });

        builder.setPositiveButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        builder.show();
       // super.onBackPressed();
    }
}
