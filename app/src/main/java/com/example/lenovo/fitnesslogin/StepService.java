package com.example.lenovo.fitnesslogin;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.IBinder;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Date;

public class StepService extends Service implements SensorEventListener{

    DatabaseReference db;
    DatabaseReference dbchild;
    String date;
    SensorManager senSensorManager;
    Sensor senAccelerometer;
    long lastUpdate = 0;
    float last_x, last_y, last_z;
    static final int SHAKE_THRESHOLD = 2500;
    int numStep,calories;

    public StepService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        String id = FirebaseAuth.getInstance().getCurrentUser().getUid();
        db = FirebaseDatabase.getInstance().getReference("Progress Report").child(id);
        date = new SimpleDateFormat("d-MM-yyyy").format(new Date());
        dbchild = db.child(date).child("Step Tracker");


        senSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        senAccelerometer = senSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        senSensorManager.registerListener(this, senAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);

    }

    @Override
    public void onStart(Intent intent, int startId) {

        dbchild.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if(dataSnapshot.child("Calories").exists())
                {
                    numStep = dataSnapshot.child("Steps").getValue(int.class);
                    calories = dataSnapshot.child("Calories").getValue(int.class);



                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        super.onStart(intent, startId);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {

        Sensor mySensor = sensorEvent.sensor;

        if (mySensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            float x = sensorEvent.values[0];
            float y = sensorEvent.values[1];
            float z = sensorEvent.values[2];

            long curTime = System.currentTimeMillis();

            if ((curTime - lastUpdate) > 100) {
                long diffTime = (curTime - lastUpdate);
                lastUpdate = curTime;

                float speed = Math.abs(x + y + z - last_x - last_y - last_z)/ diffTime * 100000;

                if (speed > SHAKE_THRESHOLD) {

                    getStepCount();
                }

                last_x = x;
                last_y = y;
                last_z = z;
            }
        }

    }



    private void getStepCount() {

        numStep++;

        calories = numStep / 20;

        dbchild.child("Steps").setValue(numStep);
        dbchild.child("Calories").setValue(calories);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    @Override
    public void onDestroy() {

        senSensorManager.unregisterListener(this);

        super.onDestroy();
    }
}
