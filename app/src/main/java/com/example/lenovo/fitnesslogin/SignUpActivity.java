package com.example.lenovo.fitnesslogin;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class SignUpActivity extends AppCompatActivity {

    // private FirebaseDatabase database;
    public DatabaseReference databaseUser;
    EditText cname,username,pass,cpass;
    FirebaseAuth firebaseAuth;
    RadioButton r1,r2;
    ProgressDialog progressDialog;
    String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View _mw = getLayoutInflater().inflate(R.layout.activity_sign_up, null);
        setContentView(_mw);
        cname = (EditText) findViewById(R.id.Name);
        username = (EditText)findViewById(R.id.uname);

        firebaseAuth = FirebaseAuth.getInstance();

        if(firebaseAuth.getCurrentUser() != null)
        {
            finish();
            startActivity(new Intent(this,MyActivity.class));
        }

        pass = (EditText)findViewById(R.id.new_pass);
        cpass = (EditText)findViewById(R.id.confirm_pass);
        r1 = (RadioButton)findViewById(R.id.radioButton1);
        r2 = (RadioButton)findViewById(R.id.radioButton2);
        progressDialog = new ProgressDialog(this);

        databaseUser = FirebaseDatabase.getInstance().getReference("Registration Data");
    }

    public void onCreateAccount(View view) {

        registeruser();
    }

    //final DatabaseReference name = myRef.child("Name");
    //name.push().setValue(edemail.getText().toString());


    public void registeruser()
    {
        String Name = cname.getText().toString().trim();
        String User_name = username.getText().toString().trim();
        String password = pass.getText().toString().trim();
        if(!TextUtils.isEmpty(Name)&&!TextUtils.isEmpty(User_name)&&!TextUtils.isEmpty(password))
        {
            if(pass.getText().toString().equals(cpass.getText().toString()))
            {

                progressDialog.setMessage("Registering and varifying....");
                progressDialog.show();
                firebaseAuth.createUserWithEmailAndPassword(User_name, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Verification();

                            progressDialog.dismiss();
                            finish();
                            startActivity(new Intent(getApplicationContext(), SplashFirst.class));
                        }
                    }
                });

               id = databaseUser.push().getKey();

                //DatabaseReference sign = databaseUser.child("Data");

                if (pass.getText().toString().equals(cpass.getText().toString())) {
                    if (r1.isChecked()) {

                        databaseUser.push().setValue("Male");

                    } else {
                        databaseUser.push().setValue("Female");
                    }


                    User user = new User(Name, User_name, password);


                    databaseUser.child(id).setValue(user);
                    Toast.makeText(this,"Completed", Toast.LENGTH_SHORT).show();


                    //  sign.push().setValue();
                    // sign.push().setValue(username.getText().toString());
                    // sign.push().setValue(pass.getText().toString());

                    Toast.makeText(getApplicationContext(), "You have successfully Signup", Toast.LENGTH_LONG).show();
                    Intent i1 = new Intent(this, SplashFirst.class);
                    startActivity(i1);
                }
            }
            else
            {
                Toast.makeText(getApplicationContext(),"Password mismatch",Toast.LENGTH_LONG).show();
            }

        }
        else
        {
            Toast.makeText(this,"You should fill all fields",Toast.LENGTH_LONG).show();
        }

    }



    private void Verification() {

        FirebaseUser user1 = firebaseAuth.getCurrentUser();
        user1.sendEmailVerification()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if(task.isSuccessful())
                        {
                            Toast.makeText(SignUpActivity.this, "Check youe E-mail for varificarion", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

}
