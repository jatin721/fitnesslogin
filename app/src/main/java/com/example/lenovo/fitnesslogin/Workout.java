package com.example.lenovo.fitnesslogin;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import static android.R.attr.data;
import static android.R.attr.start;


/**
 * A simple {@link Fragment} subclass.
 */
public class Workout extends Fragment {


    SearchView searchView;
    ListView lv;

    public Workout() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View rootview = inflater.inflate(R.layout.fragment_workout,container,false);
        ListView listView = rootview.findViewById(R.id.actList);


        String[] data = {"Arm Workout", "Full body Workout","Abs Workout","Leg Workout"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, data);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if(i == 0)
                {
                    startActivity(new Intent(getActivity(),ActivityArm.class));


                }else if(i == 1)
                {

                    startActivity(new Intent(getActivity(),FullMale1.class));

                }else if(i == 2)
                {
                    startActivity(new Intent(getActivity(),ActivityAbs.class));
                    //Toast.makeText(getActivity(),"U selected 3",Toast.LENGTH_SHORT).show();

                }
                else if(i == 3)
                {
                    startActivity(new Intent(getActivity(),ActivityLeg.class));
                   // Toast.makeText(getActivity(),"U selected 4",Toast.LENGTH_SHORT).show();
                }
            }
        });

        return rootview;
    }




}
