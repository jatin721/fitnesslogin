package com.example.lenovo.fitnesslogin;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class MainActivity extends AppCompatActivity {

    private EditText edemail,edpassword;
    private Button log;
    private ProgressDialog progressDialog;
    // private FirebaseDatabase database;
    private DatabaseReference dataRef;

    public FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthStateListener;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progressDialog = new ProgressDialog(this);
        edemail = (EditText) findViewById(R.id.editText);
        edpassword = (EditText) findViewById(R.id.editText2);
        log = (Button) findViewById(R.id.login);

        mAuth = FirebaseAuth.getInstance();
        if(mAuth.getCurrentUser() != null)
        {
            finish();
            startActivity(new Intent(this,MyActivity.class));
        }
        dataRef = FirebaseDatabase.getInstance().getReference("Registration Data");


    }



    public void onSignup(View view)
    {
        Intent intent = new Intent(MainActivity.this,SignUpActivity.class);
        startActivity(intent);
    }

    public void onLogin(View view) {

        String Email = edemail.getText().toString().trim();
        String Password = edpassword.getText().toString().trim();

        if(!TextUtils.isEmpty(Email) && !TextUtils.isEmpty(Password))
        {
            progressDialog.setMessage("Signing to your account");
            progressDialog.show();

            mAuth.signInWithEmailAndPassword(Email,Password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    progressDialog.dismiss();

                    if (task.isSuccessful())
                    {
                        finish();
                        startActivity(new Intent(getApplicationContext(),MyActivity.class));
                    }
                    else {
                        Toast.makeText(getApplicationContext(),"Check ur Email/Password",Toast.LENGTH_LONG).show();
                    }
                }
            });
        }
        else {
            Toast.makeText(getApplicationContext(),"Please enter empty fields",Toast.LENGTH_LONG).show();
        }

    }

    public void onForgot(View view) {

        startActivity(new Intent(getApplicationContext(),forgetPassword.class));
    }

    @Override
    public void onBackPressed() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setMessage("Do you want to Exit ?");
        builder.setCancelable(false);

        builder.setNegativeButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                finish();


            }
        });
        builder.setPositiveButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                dialogInterface.cancel();


            }
        });

        builder.show();

    }
}





//@Override
//  protected void onStart() {
//  super.onStart();

//            dataRef.addValueEventListener(new ValueEventListener() {
//              @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {

//              for(DataSnapshot userSnapshot : dataSnapshot.getChildren()){

// User user = userSnapshot.getValue(User.class);
// System.out.println(user);
//            }
//      }

//    @Override
//  public void onCancelled(DatabaseError databaseError) {

//}
//});

//}