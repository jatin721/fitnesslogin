package com.example.lenovo.fitnesslogin;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Date;

public class StepActivity extends AppCompatActivity{


    SensorManager senSensorManager;
    Sensor senAccelerometer;
    long lastUpdate = 0;
    float last_x, last_y, last_z;
    static final int SHAKE_THRESHOLD = 2500;
    int numStep=0;
    ProgressBar pb;
    int calories=0;
    TextView text,textView;

    DatabaseReference db;
    DatabaseReference dbchild;
    String date;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_step);
        Intent myIntent = new Intent(StepActivity.this,StepService.class);

        startService(myIntent);
        String id = FirebaseAuth.getInstance().getCurrentUser().getUid();
        db = FirebaseDatabase.getInstance().getReference("Progress Report").child(id);
        date = new SimpleDateFormat("d-MM-yyyy").format(new Date());
        dbchild = db.child(date).child("Step Tracker");


        pb = (ProgressBar) findViewById(R.id.progressBar2);
        pb.setMax(1000);

        text = (TextView) findViewById(R.id.textView);
        text.setText("Steps:" + numStep);

        textView = (TextView) findViewById(R.id.textView1);

        textView.setText("Calories:" + calories);


        pb.getProgressDrawable().setColorFilter(Color.CYAN, PorterDuff.Mode.SRC_IN);


        ImageButton imageButton = (ImageButton) findViewById(R.id.resetButton);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                numStep=0;
                calories=0;
                dbchild.child("Steps").setValue(numStep);
                dbchild.child("Calories").setValue(calories);
            }
        });





    }


    @Override
    protected void onStart() {
        super.onStart();


        dbchild.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if(dataSnapshot.child("Calories").exists())
                {
                    numStep = dataSnapshot.child("Steps").getValue(int.class);
                    calories = dataSnapshot.child("Calories").getValue(int.class);
                    text.setText("Steps:"+numStep);
                    textView.setText("Calories:"+calories);


                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        Intent i = new Intent(StepActivity.this,StepService.class);
        stopService(i);
    }
}
