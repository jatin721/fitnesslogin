package com.example.lenovo.fitnesslogin;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

public class FemaleLegAct2 extends AppCompatActivity {

    ImageView img;
    FloatingActionButton fab;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_female_leg_act2);

        fab = (FloatingActionButton) findViewById(R.id.fb_fhighknee);
        img = (ImageView) findViewById(R.id.femalehighknee);
        String url = "https://firebasestorage.googleapis.com/v0/b/project--5985293472804418431.appspot.com/o/image%2FFemaleWorkout%2FLeg%2FHigh%20knees.gif?alt=media&token=4e6d0942-cb99-48e2-a889-8805cefc9b28";
        Glide.with(getApplicationContext()).load(url).into(img);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(getApplicationContext(),FemaleLegAct3.class));
            }
        });
    }
}
