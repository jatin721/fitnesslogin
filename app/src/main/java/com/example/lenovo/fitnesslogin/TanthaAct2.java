package com.example.lenovo.fitnesslogin;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

public class TanthaAct2 extends AppCompatActivity {

    FloatingActionButton fab;
    ImageView img;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tantha_act2);

        img = (ImageView) findViewById(R.id.paschimoxana);
        fab = (FloatingActionButton) findViewById(R.id.fb_pachi);
        String url = "https://firebasestorage.googleapis.com/v0/b/project--5985293472804418431.appspot.com/o/image%2FYogaAct%2FTanthaYoga%2Fpaschimottasana.png?alt=media&token=63d156d5-a742-45cf-9822-892fd302739a";
        Glide.with(getApplicationContext()).load(url).into(img);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),TanthaAct3.class));
            }
        });
    }
}
