package com.example.lenovo.fitnesslogin;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

public class FemaleAbsAct3 extends AppCompatActivity {

    ImageView img;
    FloatingActionButton fab;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_female_abs_act3);

        fab = (FloatingActionButton) findViewById(R.id.fb_fAbdominal);
        img = (ImageView) findViewById(R.id.femaleAcrunch);
        String url = "https://firebasestorage.googleapis.com/v0/b/project--5985293472804418431.appspot.com/o/image%2FFemaleWorkout%2FFemaleAbs%2FABDOMINAL%20CRUNCH.gif?alt=media&token=847811cc-763f-404f-881d-2468f779b6ba";
        Glide.with(getApplicationContext()).load(url).into(img);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(getApplicationContext(),FemaleAbsAct4.class));
            }
        });
    }
}
