package com.example.lenovo.fitnesslogin;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class varificationActivity extends AppCompatActivity {

    FirebaseAuth firebaseAuth;
    EditText email;
    ProgressDialog progressDialog;
    Button varify;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_varification);

        firebaseAuth = FirebaseAuth.getInstance();
        email = (EditText) findViewById(R.id.editVarify);
        varify = (Button) findViewById(R.id.btnVarify);
        progressDialog = new ProgressDialog(this);

    }
    public void VarificationAct(View view) {

        FirebaseUser newuser = firebaseAuth.getCurrentUser();
        progressDialog.setMessage("Varifying");
        progressDialog.show();
        newuser.sendEmailVerification()
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            progressDialog.dismiss();
                            Toast.makeText(getApplicationContext(), "Check youe E-mail for varification", Toast.LENGTH_SHORT).show();
                            // startActivity(new Intent(getApplicationContext(),SignUpActivity.class));

                        }
                    }
                });



    }
}
