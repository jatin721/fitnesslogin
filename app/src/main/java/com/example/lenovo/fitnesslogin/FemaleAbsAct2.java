package com.example.lenovo.fitnesslogin;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

public class FemaleAbsAct2 extends AppCompatActivity {

    ImageView img;
    FloatingActionButton fab;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_female_abs_act2);


        fab = (FloatingActionButton) findViewById(R.id.fb_frightcrunch);
        img = (ImageView) findViewById(R.id.femalecrunchright);
        String url = "https://firebasestorage.googleapis.com/v0/b/project--5985293472804418431.appspot.com/o/image%2FFemaleWorkout%2FFemaleAbs%2FAb%20crunch%20with%20leg%20lifted%20(right).gif?alt=media&token=3f1ccd40-063f-4087-b3e8-d6d6b9bf3404";
        Glide.with(getApplicationContext()).load(url).into(img);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(getApplicationContext(),FemaleAbsAct3.class));
            }
        });
    }
}
