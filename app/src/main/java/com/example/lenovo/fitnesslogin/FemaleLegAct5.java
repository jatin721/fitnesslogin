package com.example.lenovo.fitnesslogin;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.Date;

public class FemaleLegAct5 extends AppCompatActivity {

    DatabaseReference databaseReference,dbchild;
    ImageView img;
    FloatingActionButton fab;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_female_leg_act5);
        fab = (FloatingActionButton) findViewById(R.id.fb_fstep);
        img = (ImageView) findViewById(R.id.femalestep);
        String url = "https://firebasestorage.googleapis.com/v0/b/project--5985293472804418431.appspot.com/o/image%2FFemaleWorkout%2FLeg%2FStep-up.gif?alt=media&token=5b41a0a9-5641-4170-9fbb-d8df0083dfca";
        Glide.with(getApplicationContext()).load(url).into(img);

        fab.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(FemaleLegAct5.this);
                builder.setMessage("Do you want to Save ?");
                builder.setCancelable(false);

                builder.setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        save();
                        startActivity(new Intent(getApplicationContext(),FemaleMainAct.class));

                    }
                });
                builder.setPositiveButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        finish();
                        startActivity(new Intent(getApplicationContext(),FemaleMainAct.class));

                    }
                });

                builder.show();

            }

        });

    }

    public void save()
    {
        String id = FirebaseAuth.getInstance().getCurrentUser().getUid();
        databaseReference = FirebaseDatabase.getInstance().getReference("Progress Report").child(id);
        String date = new SimpleDateFormat("d-MM-yyyy").format(new Date());
        dbchild = databaseReference.child(date).child("Activity");
        dbchild.child("Leg Workout(f)").setValue("completed");

        Toast.makeText(getApplicationContext(),"Saved",Toast.LENGTH_SHORT).show();
    }
}
