package com.example.lenovo.fitnesslogin;

/**
 * Created by jatin on 29-10-2017.
 */

public class User {

    String c_UID;
    String c_name;
    String c_uname;
   // String c_gender;
    String c_pass;

    public User()
    {

    }

    public User(String c_name, String c_uname, String c_pass) {

        this.c_name = c_name;
        this.c_uname = c_uname;
       // this.c_gender = c_gender;
        this.c_pass = c_pass;
    }

    public String getC_UID() {
        return c_UID;
    }

    public String getC_name() {
        return c_name;
    }

    public String getC_uname() {
        return c_uname;
    }

   // public String getC_gender() {
  //      return c_gender;
  //  }

    public String getC_pass() {
        return c_pass;
    }

    public void setC_UID(String c_UID) {
        this.c_UID = c_UID;
    }

    public void setC_name(String c_name) {
        this.c_name = c_name;
    }

    public void setC_uname(String c_uname) {
        this.c_uname = c_uname;
    }

    public void setC_pass(String c_pass) {
        this.c_pass = c_pass;
    }
}



