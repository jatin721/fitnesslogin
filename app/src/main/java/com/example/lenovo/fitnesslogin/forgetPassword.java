package com.example.lenovo.fitnesslogin;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

public class forgetPassword extends AppCompatActivity {


    ProgressDialog progressDialog;
    FirebaseAuth firebaseAuth;
    EditText NewEmail;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View _mw = getLayoutInflater().inflate(R.layout.activity_forget_password, null);
        setContentView(_mw);

        progressDialog = new ProgressDialog(this);
        firebaseAuth = FirebaseAuth.getInstance();
        NewEmail = (EditText) findViewById(R.id.editmail);
    }

    public void SendEmailActivity(View view) {

        String newmail = NewEmail.getText().toString().trim();

        progressDialog.setMessage("Please wait");
        progressDialog.show();
         firebaseAuth.sendPasswordResetEmail(newmail).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {

                            if (task.isSuccessful()) {
                                progressDialog.dismiss();
                                Toast.makeText(getApplicationContext(), "We have sent you instructions to reset your password", Toast.LENGTH_LONG).show();
                                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                            } else {
                                progressDialog.dismiss();
                                Toast.makeText(getApplicationContext(), "Sorry this email not registered to our app", Toast.LENGTH_LONG).show();

                            }
                        }
                    });



        }
    }
