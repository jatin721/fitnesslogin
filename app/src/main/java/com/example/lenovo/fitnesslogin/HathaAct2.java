package com.example.lenovo.fitnesslogin;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

public class HathaAct2 extends AppCompatActivity {

    FloatingActionButton fab;
    ImageView img;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hatha_act2);


        img = (ImageView) findViewById(R.id.bidalasana);
        fab = (FloatingActionButton) findViewById(R.id.fb_bidal);
        String url = "https://firebasestorage.googleapis.com/v0/b/project--5985293472804418431.appspot.com/o/image%2FYogaAct%2FHathaYoga%2Fbidalasana.png?alt=media&token=6a729f1f-c91b-4075-b0c6-7f606a7757cf";
        Glide.with(getApplicationContext()).load(url).into(img);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),HathaAct3.class));
            }
        });



    }
}
