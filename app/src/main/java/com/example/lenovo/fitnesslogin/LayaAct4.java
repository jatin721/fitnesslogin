package com.example.lenovo.fitnesslogin;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

public class LayaAct4 extends AppCompatActivity {


    FloatingActionButton fab;
    ImageView img;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_laya_act4);

        img = (ImageView) findViewById(R.id.purvottasanaleft);
        fab = (FloatingActionButton) findViewById(R.id.fb_purvaleft);
        String url = "https://firebasestorage.googleapis.com/v0/b/project--5985293472804418431.appspot.com/o/image%2FYogaAct%2FLayaYoga%2FpurvottanasanaLeft.png?alt=media&token=fe16eba7-e3b1-405f-8806-4853de7c5d0e";
        Glide.with(getApplicationContext()).load(url).into(img);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),LayaAct5.class));
            }
        });
    }
}
