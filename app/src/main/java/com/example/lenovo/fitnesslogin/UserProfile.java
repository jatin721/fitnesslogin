package com.example.lenovo.fitnesslogin;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;



public class UserProfile extends AppCompatActivity {



    private TextView userInfo;
    private FirebaseAuth firebaseAuth;
    private ProgressDialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);


        userInfo = (TextView) findViewById(R.id.textUser);

        firebaseAuth = FirebaseAuth.getInstance();
        if(firebaseAuth.getCurrentUser() == null)
        {
            finish();
            startActivity(new Intent(this,MainActivity.class));
        }

        FirebaseUser user = firebaseAuth.getCurrentUser();
        userInfo.setText("Welcome\n"+user.getEmail());

    }

    public void SignOut(View view) {

        firebaseAuth.signOut();
        finish();
        startActivity(new Intent(getApplicationContext(), MainActivity.class));
    }



    public void upAct(View view) {
        startActivity(new Intent(getApplicationContext(),imageAct.class));
    }
}
